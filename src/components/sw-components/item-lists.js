import React from 'react';
import ItemList from '../item-list';
import {withData, withSwapiService, withChildFunction, compose} from '../hoc-helpers';


const renderName = ({name}) => <span>{name}</span>;
const renderModelAndName = ({model, name}) => <span>{name} ({model})</span>;
const mapPersonMenthodsToProps = (swapiService) => {
    return {
        getData: swapiService.getAllPeople,
    }
};

const mapPlanetMenthodsToProps = (swapiService) => {
    return {
        getData: swapiService.getAllPlanets,
    }
};


const mapStarshipsMenthodsToProps = (swapiService) => {
    return {
        getData: swapiService.getAllStarships,
    }
};

const PersonList = compose(
    withSwapiService(mapPersonMenthodsToProps),
    withData,
    withChildFunction(renderName),
)(ItemList);

const PlanetList = compose(
    withSwapiService(mapPlanetMenthodsToProps),
    withData,
    withChildFunction(renderName),
)(ItemList);

const StarshipList = compose(
    withSwapiService(mapStarshipsMenthodsToProps),
    withData,
    withChildFunction(renderModelAndName),
)(ItemList);

export {
    PersonList,
    PlanetList,
    StarshipList
};
