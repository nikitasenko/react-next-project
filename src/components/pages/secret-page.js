import React from 'react';
import {Redirect} from 'react-router-dom';

const SercetPage = ({isLoggedIn}) => {
	if (isLoggedIn) {
		return (
			<div className={'jumbotron'}>
				<p>SECRET PAGE</p>
			</div>
		)
	}
	return <Redirect to={'/login'}/>
};

export default SercetPage;
